# Ink Extensions

This repository contains a collection of many useful Inkscape extensions.

To clone this repository, including its submodules, also updating each submodule to its latest version, do: 

`git clone --recurse-submodules https://gitlab.com/Moini/ink_extensions.git`

Each included third-party repository has its own directory structure, and some have dependencies that are not included. Be sure to read the individual extension's documentation.

Please note each extension's license before you use, edit or redistribute it.

If you have an extension that you would like to see added here, would like to expand an extension's description in this README file, or would like to otherwise see this repository updated, please [open an issue](https://gitlab.com/Moini/ink_extensions/issues) for it.


## Contents


### Bezier-Envelope-for-Inkscape

**Author:**  
**Description:**  
**URL:**  
**Inkscape versions:**  
**Operating systems:**  
**Menu location:**  
**Last tested:**  (commit hash, date) by (tester)  
**License:**  
Screenshot

---

### Cubify

---

### EggBot

---

### HeightMapMaker

---

### Inkscape-JPEG-export-extension

---

### Inkscape_LivingHinge

---

### SheetMetalCone

---

### TabbedBoxMaker

---

### WriteTeX

---

### XeTexText

---

### beautyfullday_fibonaccipattern

---

### countersheetsextension

---

### elliptical-box-maker

---

### hexmapextension

---

### imagealigner

---

### inkTan

---

### ink_line_animator

---

### inkdatatable

---

### inkpacking

---

### inkscape

---

### inkscape-LasercutBox

---

### inkscape-Zoetrope

---

### inkscape-android-export

---

### inkscape-applytransforms

---

### inkscape-bobbinlace

---

### inkscape-centerline-trace

---

### inkscape-chain-paths

---

### inkscape-checkerboard

---

### inkscape-gears-dev

---

### inkscape-guide-tools

---

### inkscape-jigsaw

---

### inkscape-music-scale-generator

---

### inkscape-paths2openscad

---

### inkscape-placeholder

---

### inkscape-realscale-extension

---

### inkscape-silhouette

---

### inkscape-speleo

---

### inkscape-sudoku

---

### inkscape-text2hershey

---

### inkscape-underlinetext

---

### inkscapeCartesianAxes2D

---

### inkscapeCartesianPlotData2D

---

### inkscapeCartesianPlotFunction2D

---

### inkscapeCartesianStemPlot

---

### inkscapeCircuitSymbols

---

### inkscapeCreateMarkers

---

### inkscapeLogicGates

---

### inkscapeMadeEasy

---

### inkscapePolarAxes2D

---

### inkscape_dimensioning

---

### inkscape_extension_template

---

### inkscape_fret_ruler

---

### inkscape_generator

---

### inkscape_glue_extension

---

### inkstitch

---

### inx-attributelabels

---

### inx-exportobjects

---

### inx-modifycolor

---

### inx-modifyimage

---

### inx-pathops

---

### inx-pixel2svg

---

### svgo-inkscape

---

### textext

---

### uuid_labeller

---

### fix-text-lines
